module WhiteNicksIds;

import core.sys.windows.windows;
import core.sys.windows.dll;
import core.runtime;
import std.concurrency : spawn, yield;
import core.stdc.string : memcpy;
import core.stdc.stdio : snprintf;

extern (Windows) BOOL DllMain(HINSTANCE hInstance, ULONG ulReason, LPVOID) {
	final switch (ulReason) {
		case DLL_PROCESS_ATTACH:
			Runtime.initialize();
			spawn({
				while (*cast(uint*)0xC8D4C0 < 9)
					yield();
				samp = cast(DWORD)GetModuleHandleA("samp");
				isR1 = *cast(ubyte *)( samp + 0x129 ) == 0xF4;
				hook_addr = cast(void*)(samp + (isR1 ? HOOK_ADDR_R1 : HOOK_ADDR_R3));
				InstallHook();
			});
			dll_process_attach(hInstance, true);
			break;
		case DLL_PROCESS_DETACH:
			RemoveHook();
			Runtime.terminate();
			dll_process_detach(hInstance, true);
			break;
		case DLL_THREAD_ATTACH:
			dll_thread_attach(true, true);
			break;
		case DLL_THREAD_DETACH:
			dll_thread_detach(true, true);
			break;
	}
	return true;
}


void InstallHook(){
    auto relative_addr = cast(DWORD)&name_hook - (cast(DWORD)hook_addr + HOOK_LEN);
    DWORD vp;
    hook_addr.VirtualProtect(HOOK_LEN, PAGE_EXECUTE_READWRITE, &vp);
    *cast(DWORD*)(hook_addr + 1) = relative_addr;
    hook_addr.VirtualProtect(HOOK_LEN, vp, &vp);	
}

void RemoveHook(){
	auto hook_code = cast(char*)(isR1 ? HOOK_CODE_R1 : HOOK_CODE_R3);
    DWORD vp;
    hook_addr.VirtualProtect(HOOK_LEN, PAGE_EXECUTE_READWRITE, &vp);	
    hook_addr.memcpy(hook_code, HOOK_LEN);
    hook_addr.VirtualProtect(HOOK_LEN, vp, &vp);
}

private {
	extern (C) auto name_hook( char* buf, const(char*) fmt, const(char*) nick, const(DWORD) ID ) @nogc{
		return buf.snprintf(128, "%s {FFFFFF}(%03d)", nick, ID);
	}

	const HOOK_ADDR_R1 = 0x70F4E;
	const HOOK_ADDR_R3 = 0x74E3F;
	const HOOK_LEN = 5;
	const HOOK_CODE_R1 = "\xE8\xA0\x4C\x04\x00";
	const HOOK_CODE_R3 = "\xE8\x4F\x2D\x05\x00";

	__gshared DWORD samp;
	__gshared bool isR1;
	__gshared void* hook_addr;
}
